# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:28+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: vänsterknapp"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: högerknapp"

#: ../../<rst_epilog>:6
msgid ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mousemiddle"
msgstr ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mittenknapp"

#: ../../<rst_epilog>:82
msgid ""
".. image:: images/icons/zoom_tool.svg\n"
"   :alt: toolzoom"
msgstr ""
".. image:: images/icons/zoom_tool.svg\n"
"   :alt: zoomverktyg"

#: ../../reference_manual/tools/zoom.rst:1
msgid "Krita's zoom tool reference."
msgstr "Referens för Kritas zoomverktyg."

#: ../../reference_manual/tools/zoom.rst:11
msgid "Tools"
msgstr "Verktyg"

#: ../../reference_manual/tools/zoom.rst:11
msgid "Zoom"
msgstr "Zoom"

#: ../../reference_manual/tools/zoom.rst:16
msgid "Zoom Tool"
msgstr "Zoomverktyg"

#: ../../reference_manual/tools/zoom.rst:18
msgid "|toolzoom|"
msgstr "|toolzoom|"

#: ../../reference_manual/tools/zoom.rst:20
msgid ""
"The zoom tool allows you to zoom your canvas in and out discretely. It can "
"be found at the bottom of the toolbox, and you just activate it by selecting "
"the tool, and doing |mouseleft| on the canvas will zoom in, while |"
"mouseright| will zoom out."
msgstr ""
"Zoomverktyget låter dig zooma in eller ut duken i diskreta steg. Man hittar "
"det längst ner i verktygslådan, och man aktiverar det helt enkelt genom att "
"välja verktyget, och vänsterklick på duken zoomar in medan högerklick zoomar "
"ut."

#: ../../reference_manual/tools/zoom.rst:22
msgid "You can reverse this behavior in the :guilabel:`Tool Options`."
msgstr "Beteendet kan omvändas i :guilabel:`Verktygsalternativ`."

#: ../../reference_manual/tools/zoom.rst:24
msgid ""
"There's a number of hotkeys associated with this tool, which makes it easier "
"to access from the other tools:"
msgstr ""
"Det finns ett antal snabbtangenter som hör ihop med verktyget, som gör det "
"enklare att komma åt från andra verktyg:"

#: ../../reference_manual/tools/zoom.rst:26
msgid ""
":kbd:`Ctrl + Space +` |mouseleft| :kbd:`+ drag` on the canvas will zoom in "
"or out fluently."
msgstr ""
":kbd:`Ctrl + mellanslag +` vänsterknapp + dra på duken zoomar in eller ut "
"smidigt."

#: ../../reference_manual/tools/zoom.rst:27
msgid ""
":kbd:`Ctrl +` |mousemiddle| :kbd:`+ drag` on the canvas will zoom in or out "
"fluently."
msgstr ":kbd:`Ctrl +` mittenknapp + dra på duken zoomar in eller ut smidigt."

#: ../../reference_manual/tools/zoom.rst:28
msgid ""
":kbd:`Ctrl + Alt + Space +` |mouseleft| :kbd:`+ drag` on the canvas will "
"zoom in or out with discrete steps."
msgstr ""
":kbd:`Ctrl + Alt + mellanslag +` vänsterknapp + dra på duken zoomar in eller "
"ut med diskreta steg."

#: ../../reference_manual/tools/zoom.rst:29
msgid ""
":kbd:`Ctrl + Alt +` |mousemiddle| :kbd:`+ drag` on the canvas will zoom in "
"or out with discrete steps."
msgstr ""
":kbd:`Ctrl + Alt +` mittenknapp + dra på duken zoomar in eller ut med "
"diskreta steg."

#: ../../reference_manual/tools/zoom.rst:30
msgid ":kbd:`+` will zoom in with discrete steps."
msgstr ":kbd:`+` zoomar in med diskreta steg."

#: ../../reference_manual/tools/zoom.rst:31
msgid ":kbd:`-` will zoom out with discrete steps."
msgstr ":kbd:`-` zoomar ut med diskreta steg."

#: ../../reference_manual/tools/zoom.rst:32
msgid ":kbd:`1` will set the zoom to 100%."
msgstr ":kbd:`1` ställer in zoomnivån till 100 %."

#: ../../reference_manual/tools/zoom.rst:33
msgid ""
":kbd:`2` will set the zoom so that the document fits fully into the canvas "
"area."
msgstr ""
":kbd:`2` ställer in zoomnivån så att hela dokumentet får plats helt och "
"hållet på duken."

#: ../../reference_manual/tools/zoom.rst:34
msgid ""
":kbd:`3` will set the zoom so that the document width fits fully into the "
"canvas area."
msgstr ""
":kbd:`3` ställer in zoomnivån så att hela dokumentets bredd får plats helt "
"och hållet på duken."

#: ../../reference_manual/tools/zoom.rst:36
msgid "For more information on such hotkeys, check :ref:`navigation`."
msgstr ""
"För mer information om sådana snabbtangenter, ta en titt på :ref:"
"`navigation`."
