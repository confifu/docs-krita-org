# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 10:51+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../general_concepts/file_formats/file_bmp.rst:1
msgid "The Bitmap file format."
msgstr "Het bestandsformaat Bitmap."

#: ../../general_concepts/file_formats/file_bmp.rst:10
msgid "*.bmp"
msgstr "*.bmp"

#: ../../general_concepts/file_formats/file_bmp.rst:10
msgid "BMP"
msgstr "BMP"

#: ../../general_concepts/file_formats/file_bmp.rst:10
msgid "Bitmap Fileformat"
msgstr "Bestandsformaat Bitmap"

#: ../../general_concepts/file_formats/file_bmp.rst:15
msgid "\\*.bmp"
msgstr "\\*.bmp"

#: ../../general_concepts/file_formats/file_bmp.rst:17
msgid ""
"``.bmp``, or Bitmap, is the simplest raster file format out there, and, "
"being patent-free, most programs can open and save bitmap files."
msgstr ""
"``.bmp`` of bitmap, is het eenvoudigste rasterbestandsformaat beschikbaar "
"en, zonder patent, kunnen de meeste programma's bitmapbestanden openen en "
"opslaan."

#: ../../general_concepts/file_formats/file_bmp.rst:19
msgid ""
"However, most programs don't compress bitmap files, leading to BMP having a "
"reputation for being very heavy. If you need a lossless file format, we "
"actually recommend :ref:`file_png`."
msgstr ""
"De meeste programma's comprimeren bitmapbestanden niet, wat leidt tot een "
"reputatie, dat BMP zeer zwaar is. Als u een bestandsformaat zonder verlies "
"nodig heeft, dan bevelen we in werkelijkheid aan :ref:`file_png`."
