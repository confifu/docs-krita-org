# translation of docs_krita_org_reference_manual___layers_and_masks.po to Slovak
# Roman Paholík <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___layers_and_masks\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-21 12:42+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/layers_and_masks.rst:5
msgid "Layers and Masks"
msgstr "Vrstvy a masky"

#: ../../reference_manual/layers_and_masks.rst:7
msgid "Layers are a central concept in digital painting."
msgstr "Vrstvy sú hlavným konceptom v digitálnej maľbe."

#: ../../reference_manual/layers_and_masks.rst:9
msgid ""
"With layers you can get better control over your artwork, for example you "
"can color an entire artwork just by working on the separate color layer and "
"thereby not destroying the line art which will reside above this color layer."
msgstr ""

#: ../../reference_manual/layers_and_masks.rst:11
msgid ""
"Furthermore, layers allow you to change the composition easier, and mass "
"transform certain elements at once."
msgstr ""

#: ../../reference_manual/layers_and_masks.rst:13
msgid ""
"Masks on the other hand allow you to selectively apply certain effects on a "
"layer, like transparency, transformation and filters."
msgstr ""

#: ../../reference_manual/layers_and_masks.rst:15
msgid "Check the :ref:`layers_and_masks` for more information."
msgstr ""
