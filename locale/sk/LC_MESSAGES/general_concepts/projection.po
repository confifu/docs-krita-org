# translation of docs_krita_org_general_concepts___projection.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_general_concepts___projectionm\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-03-01 14:36+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../general_concepts/projection.rst:None
#, fuzzy
#| msgid ".. image:: images/en/category_projection/projection-cube_09.svg"
msgid ".. image:: images/category_projection/projection-cube_09.svg"
msgstr ".. image:: images/en/category_projection/projection-cube_09.svg"

#: ../../general_concepts/projection.rst:1
msgid "The Perspective Projection Category."
msgstr ""

#: ../../general_concepts/projection.rst:15
msgid "Perspective Projection"
msgstr "Perspektívna projekcia"

#: ../../general_concepts/projection.rst:17
msgid ""
"The Perspective Projection tutorial is one of the Kickstarter 2015 tutorial "
"rewards. It's about something that humanity has known scientifically for a "
"very long time, and decent formal training will teach you about this. But I "
"think there are very very few tutorials about it in regard to how to achieve "
"it in digital painting programs, let alone open source."
msgstr ""

#: ../../general_concepts/projection.rst:19
msgid ""
"The tutorial is a bit image heavy, and technical, but I hope the skill it "
"teaches will be really useful to anyone trying to get a grasp on a "
"complicated pose. Enjoy, and don't forget to thank `Raghukamath <https://www."
"raghukamath.com/>`_ for choosing this topic!"
msgstr ""

#: ../../general_concepts/projection.rst:24
msgid "Parts:"
msgstr "Časti:"
