msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-19 15:20+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: Krita Wavelets\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../reference_manual/filters/enhance.rst:1
msgid "Overview of the enhance filters."
msgstr "Introdução aos filtros de melhorias."

#: ../../reference_manual/filters/enhance.rst:10
#: ../../reference_manual/filters/enhance.rst:19
msgid "Sharpen"
msgstr "Afiamento"

#: ../../reference_manual/filters/enhance.rst:10
msgid "Filters"
msgstr "Filtros"

#: ../../reference_manual/filters/enhance.rst:15
msgid "Enhance"
msgstr "Melhorar"

#: ../../reference_manual/filters/enhance.rst:17
msgid ""
"These filters all focus on reducing the blur in the image by sharpening and "
"enhancing details and the edges. Following are various sharpen and enhance "
"filters in provided in Krita."
msgstr ""
"Estes filtros focam-se todos na redução dos borrões das imagens, através do "
"aumento da nitidez e dos detalhes e arestas. Seguem-se vários filtros de "
"afiamento ou nitidez e melhorias que são fornecidos com o Krita."

#: ../../reference_manual/filters/enhance.rst:20
msgid "Mean Removal"
msgstr "Remoção da Média"

#: ../../reference_manual/filters/enhance.rst:21
msgid "Unsharp Mask"
msgstr "Máscara Não-Afiada"

#: ../../reference_manual/filters/enhance.rst:22
msgid "Gaussian Noise reduction"
msgstr "Redução de ruído gaussiana"

#: ../../reference_manual/filters/enhance.rst:23
msgid "Wavelet Noise Reducer"
msgstr "Redução de Ruído por 'Wavelets'"
