# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-05-17 16:43+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Krita\n"

#: ../../<generated>:1
msgid "Angle"
msgstr "Ângulo"

#: ../../reference_manual/clones_array.rst:1
msgid "The Clones Array functionality in Krita"
msgstr "A funcionalidade de Lista de Clones no Krita"

#: ../../reference_manual/clones_array.rst:10
#: ../../reference_manual/clones_array.rst:15
msgid "Clones Array"
msgstr "Lista de Clones"

#: ../../reference_manual/clones_array.rst:10
msgid "Clone"
msgstr "Clonar"

#: ../../reference_manual/clones_array.rst:17
msgid ""
"Allows you to create a set of clone layers quickly. These are ordered in "
"terms of rows and columns. The default options will create a 2 by 2 grid. "
"For setting up tiles of an isometric game, for example, you'd want to set "
"the X offset of the rows to half the value input into the X offset for the "
"columns, so that rows are offset by half. For a hexagonal grid, you'd want "
"to do the same, but also reduce the Y offset of the grids by the amount of "
"space the hexagon can overlap with itself when tiled."
msgstr ""
"Permite-lhe criar um conjunto de camadas de clonagem de forma rápida. Estas "
"estão dispostas em termos de linhas e colunas. As opções predefinidas irão "
"criar uma grelha de 2 por 2. Para configurar as peças de um jogo isométrico, "
"por exemplo, iria querer definir a posição em X das linhas como metade do "
"valor do deslocamento do X para as colunas, para que as linhas fiquem "
"deslocadas pela metade. Para uma grelha hexagonal, iria fazer o mesmo, mas "
"também reduzir o deslocamento em Y das grelhas pela quantidade de espaço com "
"que o hexágono se poderá sobrepor a si próprio quando replicado."

#: ../../reference_manual/clones_array.rst:19
msgid "\\- Elements"
msgstr "\\- Elementos"

#: ../../reference_manual/clones_array.rst:20
msgid ""
"The amount of elements that should be generated using a negative of the "
"offset."
msgstr ""
"A quantidade de elementos que deverá ser gerada com um negativo da posição."

#: ../../reference_manual/clones_array.rst:21
msgid "\\+ Elements"
msgstr "\\+ Elementos"

#: ../../reference_manual/clones_array.rst:22
msgid ""
"The amount of elements that should be generated using a positive of the "
"offset."
msgstr ""
"A quantidade de elementos que deverá ser gerada com um positivo da posição."

#: ../../reference_manual/clones_array.rst:23
msgid "X offset"
msgstr "Deslocamento em X"

#: ../../reference_manual/clones_array.rst:24
msgid ""
"The X offset in pixels. Use this in combination with Y offset to position a "
"clone using Cartesian coordinates."
msgstr ""
"O deslocamento no eixo dos X em pixels. Use isto em conjunto com o "
"deslocamento em Y para posicionar um clone com coordenadas Cartesianas."

#: ../../reference_manual/clones_array.rst:25
msgid "Y offset"
msgstr "Deslocamento em Y"

#: ../../reference_manual/clones_array.rst:26
msgid ""
"The Y offset in pixels. Use this in combination with X offset to position a "
"clone using Cartesian coordinates."
msgstr ""
"O deslocamento no eixo dos Y em pixels. Use isto em conjunto com o "
"deslocamento em X para posicionar um clone com coordenadas Cartesianas."

#: ../../reference_manual/clones_array.rst:27
msgid "Distance"
msgstr "Distância"

#: ../../reference_manual/clones_array.rst:28
msgid ""
"The line-distance of the original origin to the clones origin. Use this in "
"combination with angle to position a clone using a polar coordinate system."
msgstr ""
"A distância à linha da posição original para a origem dos clones. Use isto "
"em conjunto com o ângulo para posicionar um clone com um sistema de "
"coordenadas polares."

#: ../../reference_manual/clones_array.rst:30
msgid ""
"The angle-offset of the column or row. Use this in combination with distance "
"to position a clone using a polar coordinate system."
msgstr ""
"O deslocamento angular da coluna ou linha. Use isto em conjunto com o a "
"distância para posicionar um clone com um sistema de coordenadas polares."
