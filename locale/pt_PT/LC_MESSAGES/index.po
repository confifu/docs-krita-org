# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-05-19 23:52+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en image jpg images resourcespage gettingstarted\n"
"X-POFile-SpellExtra: Heroreference Herogeneral referencemanual version\n"
"X-POFile-SpellExtra: tutorials ref genindex faq Heroresources\n"
"X-POFile-SpellExtra: Herotutorials Herogettingstarted Photoshop Krita\n"
"X-POFile-SpellExtra: HerouserManual usermanual Herofaq generalconcepts\n"
"X-POFile-SpellExtra: ePub intropage etá\n"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_userManual.jpg"
msgstr ".. image:: images/intro_page/Hero_userManual.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_tutorials.jpg"
msgstr ".. image:: images/intro_page/Hero_tutorials.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_getting_started.jpg"
msgstr ".. image:: images/intro_page/Hero_getting_started.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_reference.jpg"
msgstr ".. image:: images/intro_page/Hero_reference.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_general.jpg"
msgstr ".. image:: images/intro_page/Hero_general.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_faq.jpg"
msgstr ".. image:: images/intro_page/Hero_faq.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_resources.jpg"
msgstr ".. image:: images/intro_page/Hero_resources.jpg"

#: ../../index.rst:5
msgid "Welcome to the Krita |version| Manual!"
msgstr "Bem-vindo(a) ao Manual do Krita |version|!"

#: ../../index.rst:7
msgid "Welcome to Krita's documentation page."
msgstr "Bem-vindo(a) à página de documentação do Krita."

#: ../../index.rst:9
msgid ""
"Krita is a sketching and painting program designed for digital artists. Our "
"vision for Development of Krita is —"
msgstr ""
"O Krita é um programa de desenho e pintura concebido para artistas digitais. "
"A nossa visão para o Desenvolvimento do Krita é —"

#: ../../index.rst:11
msgid ""
"Krita is a free and open source cross-platform application that offers an "
"end-to-end solution for creating digital art files from scratch. Krita is "
"optimized for frequent, prolonged and focused use. Explicitly supported "
"fields of painting are illustrations, concept art, matte painting, textures, "
"comics and animations. Developed together with users, Krita is an "
"application that supports their actual needs and workflow. Krita supports "
"open standards and interoperates with other applications."
msgstr ""
"O Krita é uma aplicação livre e de código aberto que oferece uma solução "
"transversal para criar ficheiros de arte digital do zero. O Krita etá "
"optimizado para um uso frequente, prolongado e focado. Os campos "
"explicitamente suportados na pintura são as ilustrações, a arte conceptual, "
"a pintura mate, as texturas, as bandas desenhadas e as animações. "
"Desenvolvido em conjunto com os utilizadores, o Krita é uma aplicação que dá "
"suporte às suas necessidades e processos actuais. O Krita suporta normas "
"abertas e interage com outras aplicações."

#: ../../index.rst:19
msgid ""
"Krita's tools are developed keeping the above vision in mind. Although it "
"has features that overlap with other raster editors its intended purpose is "
"to provide robust tool for digital painting and creating artworks from "
"scratch. As you learn about Krita, keep in mind that it is not intended as a "
"replacement for Photoshop. This means that the other programs may have more "
"features than Krita for image manipulation tasks, such as stitching together "
"photos, while Krita's tools are most relevant to digital painting, concept "
"art, illustration, and texturing. This fact accounts for a great deal of "
"Krita's design."
msgstr ""
"As ferramentas do Krita são desenvolvidas com a visão acima mencionada em "
"mente. Ainda que tenha funcionalidades que se sobreponham a outros editores "
"de imagens rasterizadas, o seu objectivo é oferecer-lhe uma ferramenta "
"robusta para a pintura digital e para criar desenhos do zero. À medida que "
"vai aprendendo sobre o Krita, tenha em mente que o mesmo não pretende ser um "
"substituto do Photoshop. Isto significa que os outros programas poderão ter "
"mais funcionalidades que o Krita para as tarefas de manipulação de imagens, "
"como o recorte de fotografias, enquanto as ferramentas do Krita são mais "
"relevantes para a pintura digital, arte conceptual, ilustrações e texturas. "
"Este facto é tido em conta para uma grande parte dos componentes do Krita."

#: ../../index.rst:28
msgid ""
"You can download this manual as an epub `here <https://docs.krita.org/en/"
"epub/KritaManual.epub>`_."
msgstr ""
"Pode obter este manual no formato ePub `aqui <https://docs.krita.org/en/epub/"
"KritaManual.epub>`_."

#: ../../index.rst:33
msgid ":ref:`user_manual`"
msgstr ":ref:`user_manual`"

#: ../../index.rst:33
msgid ":ref:`tutorials`"
msgstr ":ref:`tutorials`"

#: ../../index.rst:35
msgid ""
"Discover Krita’s features through an online manual. Guides to help you "
"transition from other applications."
msgstr ""
"Descubra as funcionalidades do Krita através de um manual 'online'. Alguns "
"guias que o ajudam a transitar de outras aplicações."

#: ../../index.rst:35
msgid ""
"Learn through developer and user generated tutorials to see Krita in action."
msgstr ""
"Aprenda com tutoriais gerados pelos programadores e utilizadores para ver o "
"Krita em acção."

#: ../../index.rst:41
msgid ":ref:`getting_started`"
msgstr ":ref:`getting_started`"

#: ../../index.rst:41
msgid ":ref:`reference_manual`"
msgstr ":ref:`reference_manual`"

#: ../../index.rst:43
msgid "New to Krita and don't know where to start?"
msgstr "É novo no Krita e não sabe por onde começar?"

#: ../../index.rst:43
msgid "A quick run-down of all of the tools that are available"
msgstr "Um percurso rápido por todas as ferramentas que estão disponíveis"

#: ../../index.rst:48
msgid ":ref:`general_concepts`"
msgstr ":ref:`general_concepts`"

#: ../../index.rst:48
msgid ":ref:`faq`"
msgstr ":ref:`faq`"

#: ../../index.rst:50
msgid ""
"Learn about general art and technology concepts that are not specific to "
"Krita."
msgstr ""
"Aprenda os conceitos gerais de arte e tecnologia que são específicos do "
"Krita."

#: ../../index.rst:50
msgid ""
"Find answers to the most common questions about Krita and what it offers."
msgstr ""
"Descubra respostas para as perguntas mais comuns sobre o Krita e o que o "
"mesmo oferece."

#: ../../index.rst:55
msgid ":ref:`resources_page`"
msgstr ":ref:`resources_page`"

#: ../../index.rst:55
msgid ":ref:`genindex`"
msgstr ":ref:`genindex`"

#: ../../index.rst:57
msgid ""
"Textures, brush packs, and python plugins to help add variety to your "
"artwork."
msgstr ""
"Texturas, pacotes de pincéis e 'plugins' em Python para ajudar a adicionar "
"variedade às suas obras de arte."

#: ../../index.rst:57
msgid "An index of the manual for searching terms by browsing."
msgstr "Um índice do manual para a pesquisa de termos por navegação."
