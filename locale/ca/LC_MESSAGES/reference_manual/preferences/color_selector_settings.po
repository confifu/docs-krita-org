# Translation of docs_krita_org_reference_manual___preferences___color_selector_settings.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-17 18:20+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../<generated>:1
msgid "Make Brush Color Bluer"
msgstr "Fa el color del pinzell més blau"

#: ../../reference_manual/preferences/color_selector_settings.rst:1
msgid "The color selector settings in Krita."
msgstr "Ajustaments per al selector de color al Krita."

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Preferences"
msgstr "Preferències"

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Settings"
msgstr "Ajustaments"

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Color Selector"
msgstr "Selector de color"

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Color"
msgstr "Color"

#: ../../reference_manual/preferences/color_selector_settings.rst:16
msgid "Color Selector Settings"
msgstr "Ajustaments per al selector de color"

#: ../../reference_manual/preferences/color_selector_settings.rst:18
msgid ""
"These settings directly affect Advanced Color Selector Dockers and the same "
"dialog box appears when the user clicks the settings button in that docker "
"as well. They also affect certain hotkey actions."
msgstr ""
"Aquestes ajustaments afectaran directament a l'acoblador Selector avançat "
"del color i apareixeran al mateix diàleg quan l'usuari faci clic al botó "
"Ajustaments en aquest acoblador. També afectaran certes accions de les "
"dreceres."

#: ../../reference_manual/preferences/color_selector_settings.rst:20
msgid ""
"This settings menu has a drop-down for Advanced Color Selector, and Color "
"Hotkeys."
msgstr ""
"Aquest menú d'ajustaments té una llista desplegable per a Selector avançat "
"del color i Dreceres de teclat per al color."

#: ../../reference_manual/preferences/color_selector_settings.rst:23
msgid "Advanced Color Selector"
msgstr "Selector avançat del color"

#: ../../reference_manual/preferences/color_selector_settings.rst:25
msgid ""
"These settings are described on the page for the :ref:"
"`advanced_color_selector_docker`."
msgstr ""
"Aquests ajustaments es descriuen a la pàgina per al :ref:"
"`advanced_color_selector_docker`."

#: ../../reference_manual/preferences/color_selector_settings.rst:28
msgid "Color Hotkeys"
msgstr "Dreceres de teclat per al color"

#: ../../reference_manual/preferences/color_selector_settings.rst:30
msgid "These allow you to set the steps for the following actions:"
msgstr "Permeten establir els passos per a les següents accions:"

#: ../../reference_manual/preferences/color_selector_settings.rst:32
msgid "Make Brush Color Darker"
msgstr "Enfosqueix el color del pinzell"

#: ../../reference_manual/preferences/color_selector_settings.rst:33
msgid ""
"This is defaultly set to :kbd:`K` key and uses the :guilabel:`lightness` "
"steps. This uses luminance when possible."
msgstr ""
"De manera predeterminada establerta a la tecla :kbd:`K` i utilitza els "
"passos de la :guilabel:`Claredat`. Quan és possible utilitza la luminància."

#: ../../reference_manual/preferences/color_selector_settings.rst:34
msgid "Make Brush Color Lighter"
msgstr "Aclara el color del pinzell"

#: ../../reference_manual/preferences/color_selector_settings.rst:35
msgid ""
"This is defaultly set to :kbd:`L` key and uses the :guilabel:`lightness` "
"steps. This uses luminance when possible."
msgstr ""
"De manera predeterminada establerta a la tecla :kbd:`L` i utilitza els "
"passos de la :guilabel:`Claredat`. Quan és possible utilitza la luminància."

#: ../../reference_manual/preferences/color_selector_settings.rst:36
msgid "Make Brush Color More Saturated"
msgstr "Fa el color del pinzell més saturat"

#: ../../reference_manual/preferences/color_selector_settings.rst:37
#: ../../reference_manual/preferences/color_selector_settings.rst:39
msgid "This is defaultly unset and uses the :guilabel:`saturation` steps."
msgstr ""
"De manera predeterminada sense establir i utilitza els passos de la :"
"guilabel:`Saturació`."

#: ../../reference_manual/preferences/color_selector_settings.rst:38
msgid "Make Brush Color More Desaturated"
msgstr "Fa el color del pinzell més dessaturat"

#: ../../reference_manual/preferences/color_selector_settings.rst:40
msgid "Shift Brushcolor Hue clockwise"
msgstr "Desplaça el to del color del pinzell en sentit horari"

#: ../../reference_manual/preferences/color_selector_settings.rst:41
#: ../../reference_manual/preferences/color_selector_settings.rst:43
msgid "This is defaultly unset and uses the :guilabel:`Hue` steps."
msgstr ""
"De manera predeterminada sense establir i utilitza els passos del :guilabel:"
"`To`."

#: ../../reference_manual/preferences/color_selector_settings.rst:42
msgid "Shift Brushcolor Hue counter-clockwise"
msgstr "Desplaça el to del color del pinzell en sentit antihorari"

#: ../../reference_manual/preferences/color_selector_settings.rst:44
msgid "Make Brush Color Redder"
msgstr "Fa el color del pinzell més vermell"

#: ../../reference_manual/preferences/color_selector_settings.rst:45
#: ../../reference_manual/preferences/color_selector_settings.rst:47
msgid "This is defaultly unset and uses the :guilabel:`Redder/Greener` steps."
msgstr ""
"De manera predeterminada sense establir i utilitza els passos del :guilabel:"
"`Vermell/verd`."

#: ../../reference_manual/preferences/color_selector_settings.rst:46
msgid "Make Brush Color Greener"
msgstr "Fa el color del pinzell més verd"

#: ../../reference_manual/preferences/color_selector_settings.rst:48
msgid "Make Brush Color Yellower"
msgstr "Fa el color del pinzell més groc"

#: ../../reference_manual/preferences/color_selector_settings.rst:49
#: ../../reference_manual/preferences/color_selector_settings.rst:51
msgid "This is defaultly unset and uses the :guilabel:`Bluer/Yellower` steps."
msgstr ""
"De manera predeterminada sense establir i utilitza els passos del :guilabel:"
"`Blau/groc`."

#~ msgid ""
#~ "This is defaultly unset and uses the :guilabel:`Redder/Greener`  steps."
#~ msgstr ""
#~ "De manera predeterminada sense establir i utilitza els passos del :"
#~ "guilabel:`Vermell/verd`."
