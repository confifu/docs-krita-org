# Translation of docs_krita_org_contributors_manual___optimising_images.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: contributors_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 15:46+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../contributors_manual/optimising_images.rst:1
msgid "How to make and optimise images for use in the manual."
msgstr "Com crear i optimitzar les imatges per a utilitzar-les en el manual."

#: ../../contributors_manual/optimising_images.rst:10
msgid "Metadata"
msgstr "Metadades"

#: ../../contributors_manual/optimising_images.rst:10
msgid "Optimising Images"
msgstr "Optimitzar les imatges"

#: ../../contributors_manual/optimising_images.rst:15
msgid "Images for the Manual"
msgstr "Imatges per al manual"

#: ../../contributors_manual/optimising_images.rst:17
msgid ""
"This one is a little bit an extension to :ref:`saving_for_the_web`. In "
"particular it deals with making images for the manual, and how to optimise "
"images."
msgstr ""
"Aquesta és una extensió de :ref:`saving_for_the_web`. En particular, es "
"tracta de crear imatges per al manual i com optimitzar-les."

#: ../../contributors_manual/optimising_images.rst:19
msgid "Contents"
msgstr "Contingut"

#: ../../contributors_manual/optimising_images.rst:22
msgid "Tools for making screenshots"
msgstr "Eines per a crear captures de pantalla"

#: ../../contributors_manual/optimising_images.rst:24
msgid ""
"Now, if you wish to make an image of the screen with all the dockers and "
"tools, then :ref:`saving_for_the_web` won't be very helpful: It only saves "
"out the canvas contents, after all!"
msgstr ""
"Ara, si voleu crear una imatge de la pantalla amb tots els acobladors i "
"eines, llavors :ref:`saving_for_the_web` no us ajudarà gaire: després de "
"tot, només tracta sobre el contingut en el llenç!"

#: ../../contributors_manual/optimising_images.rst:26
msgid ""
"So, instead, we'll make a screenshot. Depending on your operating system, "
"there are several screenshot utilities available."
msgstr ""
"Per tant, en comptes d'això, farem una captura de pantalla. Depenent del "
"vostre sistema operatiu, hi ha disponibles diverses utilitats per a la "
"captura de pantalla."

#: ../../contributors_manual/optimising_images.rst:29
#: ../../contributors_manual/optimising_images.rst:81
msgid "Windows"
msgstr "Windows"

#: ../../contributors_manual/optimising_images.rst:31
msgid ""
"Windows has a build-in screenshot tool. It is by default on the :kbd:`Print "
"Screen` key. On laptops you will sometimes need to use the :kbd:`Fn` key."
msgstr ""
"El Windows disposa d'una eina incorporada de captura de pantalla. De manera "
"predeterminada amb la teca :kbd:`Impr Pant`. En els portàtils de vegades "
"caldrà fer servir la tecla :kbd:`Fn`."

#: ../../contributors_manual/optimising_images.rst:34
#: ../../contributors_manual/optimising_images.rst:88
#: ../../contributors_manual/optimising_images.rst:154
msgid "Linux"
msgstr "Linux"

#: ../../contributors_manual/optimising_images.rst:35
msgid ""
"Both Gnome and KDE have decent screenshot tools showing up by default when "
"using the :kbd:`Print Screen` key, as well do other popular desktop "
"environments. If, for whatever reason, you have no"
msgstr ""
"Tant el Gnome com el KDE disposen d'eines decents per a la captura de "
"pantalla que es mostren de manera predeterminada utilitzant la tecla :kbd:"
"`Impr Pant`, igual que altres entorns d'escriptori populars. Si, per alguna "
"raó, no la teniu:"

#: ../../contributors_manual/optimising_images.rst:38
msgid "With imagemagick, you can use the following command::"
msgstr "Amb ImageMagick, podreu utilitzar la següent ordre::"

#: ../../contributors_manual/optimising_images.rst:40
#: ../../contributors_manual/optimising_images.rst:219
#: ../../contributors_manual/optimising_images.rst:240
msgid "ImageMagick"
msgstr "ImageMagick"

#: ../../contributors_manual/optimising_images.rst:40
msgid "import -depth 8 -dither <filename.png>"
msgstr "import -depth 8 -dither <nom_fitxer_imatge.png>"

#: ../../contributors_manual/optimising_images.rst:42
msgid ""
"While we should minimize the amount of gifs in the manual for a variety of "
"accessibility reasons, you sometimes still need to make gifs and short "
"videos. Furthermore, gifs are quite nice to show off features with release "
"notes."
msgstr ""
"Si bé hauríem de minimitzar la quantitat de GIF en el manual per una "
"varietat de raons d'accessibilitat, de vegades encara necessitareu crear GIF "
"i vídeos curts. A més, els GIF són molt macos per a mostrar característiques "
"amb notes de llançament."

#: ../../contributors_manual/optimising_images.rst:44
msgid "For making short gifs, you can use the following programs:"
msgstr "Per a crear GIF curts, podeu utilitzar els programes següents:"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:46
msgid ""
"`Peek <https://github.com/phw/peek>`_ -- This one has an appimage and a very "
"easy user-interface. Like many screenrecording programs it does show trouble "
"on Wayland."
msgstr ""
"`Peek <https://github.com/phw/peek>`_ -- Disposa d'una «appimage» i té una "
"interfície d'usuari molt senzilla. Igual que molts programes "
"d'enregistrament de la pantalla, mostra problemes sobre Wayland."

#: ../../contributors_manual/optimising_images.rst:49
msgid "OS X"
msgstr "OS X"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:51
msgid ""
"The Screenshot hotkey on OS X is :kbd:`Shift + Command + 3`, according to "
"`the official apple documentation <https://support.apple.com/en-us/"
"HT201361>`_."
msgstr ""
"La drecera per a la captura de pantalla en OS X és :kbd:`Majús. + Ordre + "
"3`, segons `la documentació oficial d'Apple <https://support.apple.com/en-us/"
"HT201361>`_."

#: ../../contributors_manual/optimising_images.rst:54
msgid "The appropriate file format for the job"
msgstr "El format de fitxer adequat per a la tasca"

#: ../../contributors_manual/optimising_images.rst:56
msgid ""
"Different file formats are better for certain types of images. In the end, "
"we want to have images that look nice and have a low filesize, because that "
"makes the manual easier to download or browse on the internet."
msgstr ""
"Diferents formats de fitxer són millors per a certs tipus d'imatges. Al "
"final, volem tenir imatges que es vegin bé i que tinguin una mida de fitxer "
"baixa, perquè això fa que el manual sigui més fàcil de descarregar o navegar "
"sobre Internet."

#: ../../contributors_manual/optimising_images.rst:58
msgid "GUI screenshots"
msgstr "Captures de pantalla de la IGU"

#: ../../contributors_manual/optimising_images.rst:59
msgid "This should use png, and if possible, in gif."
msgstr "Aquestes haurien d'utilitzar PNG i, si és possible, GIF."

#: ../../contributors_manual/optimising_images.rst:60
msgid "Images that have a lot of flat colors."
msgstr "Imatges que tenen un munt de colors plans."

#: ../../contributors_manual/optimising_images.rst:61
msgid "This should use png."
msgstr "Aquestes haurien d'utilitzar PNG."

#: ../../contributors_manual/optimising_images.rst:62
msgid "Grayscale images"
msgstr "Imatges en escala de grisos"

#: ../../contributors_manual/optimising_images.rst:63
msgid "These should be gif or png."
msgstr "Aquestes haurien de ser en GIF o PNG."

#: ../../contributors_manual/optimising_images.rst:64
msgid "Images with a lot of gradients"
msgstr "Imatges amb un munt de degradats"

#: ../../contributors_manual/optimising_images.rst:65
msgid "These should be JPG."
msgstr "Aquestes haurien de ser en JPG."

#: ../../contributors_manual/optimising_images.rst:67
msgid "Images with a lot of transparency."
msgstr "Imatges amb un munt de transparència."

#: ../../contributors_manual/optimising_images.rst:67
msgid "These should use PNG."
msgstr "Aquestes haurien de ser en PNG."

#: ../../contributors_manual/optimising_images.rst:69
msgid ""
"The logic is the way how each of these saves colors. Jpeg is ideal for "
"photos and images with a lot of gradients because it :ref:`compresses "
"differently <lossy_compression>`. However, contrasts don't do well in jpeg. "
"PNG does a lot better with images with sharp contrasts, while in some cases "
"we can even have less than 256 colors, so gif might be better."
msgstr ""
"La lògica és la manera en què cada un d'aquests desa els colors. El JPEG és "
"ideal per a fotografies i imatges amb molts degradats perquè es :ref:"
"`comprimeix de manera diferent <lossy_compression>`. No obstant això, els "
"contrastos no funcionen bé en el JPEG. El PNG funciona molt millor amb "
"imatges amb contrastos més definits, mentre que en alguns casos fins i tot "
"podrem tenir menys de 256 colors, en aquest cas el GIF podria ser millor."

#: ../../contributors_manual/optimising_images.rst:71
msgid ""
"Grayscale images, even when they have a lot of gradients variation, should "
"be PNG. The reason is that when we use full color images, we are, depending "
"on the image, using 3 to 5 numbers to describe those values, with each of "
"those values having a possibility to contain any of 256 values. JPEG and "
"other 'lossy' file formats use clever psychological tricks to cut back on "
"the amount of values an image needs to show its contents. However, when we "
"make grayscale images, we only keep track of the lightness. The lightness is "
"only one number, that can have 256 values, making it much easier to just use "
"gif or PNG, instead of jpeg which could have nasty artifacts. (And, it is "
"also a bit smaller)"
msgstr ""
"Les imatges en escala de grisos, fins i tot quan tenen moltes variacions "
"dels degradats, haurien de ser en PNG. La raó és que quan utilitzem imatges "
"a tot color, estem, segons la imatge, utilitzant de 3 a 5 números per a "
"descriure aquests valors, i cadascun d'aquests valors té la possibilitat de "
"contenir qualsevol dels 256 valors. El JPEG i altres formats de fitxer «amb "
"pèrdua», utilitzen trucs psicològics intel·ligents per a retallar la "
"quantitat de valors que una imatge necessita per a mostrar el seu contingut. "
"No obstant això, quan creem imatges en escala de grisos, només farem un "
"seguiment de la claredat. La claredat és només un número, que pot tenir 256 "
"valors, farà que sigui molt més fàcil utilitzar només GIF o PNG, en lloc de "
"JPEG, el qual podria tenir defectes desagradables. (I, també és una mica més "
"petit)."

#: ../../contributors_manual/optimising_images.rst:73
msgid "**When in doubt, use PNG.**"
msgstr "**Quan dubteu, empreu PNG.**"

#: ../../contributors_manual/optimising_images.rst:76
msgid "Optimising Images in quality and size"
msgstr "Optimitzar les imatges en qualitat i mida"

#: ../../contributors_manual/optimising_images.rst:78
msgid ""
"Now, while most image editors try to give good defaults on image sizes, we "
"can often make them even smaller by using certain tools."
msgstr ""
"Ara, mentre la majoria d'editors d'imatges intenten donar valors "
"predeterminats bons a la mida de la imatge, sovint podem fer que encara "
"siguin més petites utilitzant determinades eines."

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:83
msgid ""
"The most commonly recommended tool for this on Windows is `IrfranView "
"<https://www.irfanview.com/>`_, but the dear writer of this document has no "
"idea how to use it exactly."
msgstr ""
"L'eina més habitualment recomanada per això a Windows és `IrfranView "
"<https://www.irfanview.com/>`_, però el volgut escriptor d'aquest document "
"no té ni idea de com utilitzar-lo amb exactitud."

#: ../../contributors_manual/optimising_images.rst:85
msgid "The other option is to use PNGCrush as mentioned in the linux section."
msgstr ""
"L'altra opció és utilitzar el PNGCrush com s'esmenta a la secció Linux."

#: ../../contributors_manual/optimising_images.rst:91
msgid "Optimising PNG"
msgstr "Optimitzar els PNG"

# skip-rule: t-acc_obe,t-apo_fin
#: ../../contributors_manual/optimising_images.rst:92
msgid ""
"There is a whole laundry list of `PNG optimisation tools <https://css-ig.net/"
"png-tools-overview>`_ available on Linux. They come in two categories: Lossy "
"(Using psychological tricks), and Lossless (trying to compress the data more "
"conventionally). The following are however the most recommended:"
msgstr ""
"Hi ha una llista completa d'eines disponibles a Linux per a l' `optimització "
"dels PNG <https://css-ig.net/png-tools-overview>`_. Vénen en dues "
"categories: amb pèrdua (emprant trucs psicològics) i sense pèrdua (intentant "
"comprimir les dades de manera més convencional). Les següents són les més "
"recomanades:"

#: ../../contributors_manual/optimising_images.rst:95
msgid ""
"A PNG compressor using lossy techniques to reduce the amount of colors used "
"in a smart way."
msgstr ""
"Un compressor de PNG que utilitza tècniques amb pèrdua per a reduir de "
"manera intel·ligent la quantitat de colors utilitzats."

#: ../../contributors_manual/optimising_images.rst:97
msgid "To use PNGquant, go to the folder of choice, and type::"
msgstr "Per utilitzar el PNGquant, aneu a la carpeta escollida i escriviu::"

#: ../../contributors_manual/optimising_images.rst:99
msgid "pngquant --quality=80-100 image.png"
msgstr "pngquant --quality=80-100 imatge.png"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:101
msgid "`PNGQuant <https://pngquant.org/>`_"
msgstr "`PNGQuant <https://pngquant.org/>`_"

#: ../../contributors_manual/optimising_images.rst:101
msgid ""
"Where *image* is replaced with the image file name. When you press the :kbd:"
"`Enter` key, a new image will appear in the folder with the compressed "
"results. PNGQuant works for most images, but some images, like the color "
"selectors don't do well with it, so always double check that the resulting "
"image looks good, otherwise try one of the following options:"
msgstr ""
"On «*imatge*» se substituirà amb el nom del fitxer d'imatge. En prémer la "
"tecla :kbd:`Retorn`, apareixerà una imatge nova a la carpeta amb el resultat "
"de la compressió. El PNGQuant funciona per a la majoria de les imatges, però "
"en algunes, com els selectors de color, no ho fan gaire bé, pel que "
"verifiqueu sempre que la imatge resultant es vegi bé; en cas contrari, "
"proveu amb una de les següents opcions:"

#: ../../contributors_manual/optimising_images.rst:104
msgid "A lossless PNG compressor. Usage::"
msgstr "Un compressor PNG sense pèrdua. Ús::"

#: ../../contributors_manual/optimising_images.rst:106
msgid "pngcrush image.png imageout.png"
msgstr "pngcrush imatge.png imatge_sortida.png"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:108
msgid "`PNGCrush <https://pmt.sourceforge.io/pngcrush/>`_"
msgstr "`PNGCrush <https://pmt.sourceforge.io/pngcrush/>`_"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:108
msgid ""
"This will try the most common methods. Add ``-brute`` to try out all methods."
msgstr ""
"Aquest provarà amb els mètodes més habituals. Afegiu ``-brute`` per a provar "
"amb tots els mètodes."

#: ../../contributors_manual/optimising_images.rst:111
msgid ""
"Another lossless PNG compressor which can be run after using PNGQuant, it is "
"apparently originally a fork of png crush. Usage::"
msgstr ""
"Un altre compressor PNG sense pèrdua, el qual es pot executar després "
"d'utilitzar el PNGQuant, aparentment és originalment una bifurcació del "
"PNGCrush. Ús::"

#: ../../contributors_manual/optimising_images.rst:114
msgid "optipng image.png"
msgstr "optipng imatge.png"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:116
msgid "`Optipng <http://optipng.sourceforge.net/>`_"
msgstr "`Optipng <http://optipng.sourceforge.net/>`_"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:116
msgid ""
"where image is the filename. OptiPNG will then proceed to test several "
"compression algorithms and **overwrite** the image.png file with the "
"optimised version. You can avoid overwriting with the ``--out imageout.png`` "
"command."
msgstr ""
"On «*imatge*» serà el nom del fitxer. Després procedirà a provar diversos "
"algorismes de compressió i **sobreescriurà** el fitxer imatge.png amb la "
"versió optimitzada. Podeu evitar sobreescriure'l amb l'ordre ``--out "
"imatge_sortida.png``."

#: ../../contributors_manual/optimising_images.rst:119
msgid "Optimising GIF"
msgstr "Optimitzar els GIF"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:121
msgid "`FFmpeg <http://blog.pkh.me/p/21-high-quality-gif-with-ffmpeg.html>`_"
msgstr "`FFmpeg <http://blog.pkh.me/p/21-high-quality-gif-with-ffmpeg.html>`_"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:122
msgid "`Gifski <https://gif.ski/>`_"
msgstr "`Gifski <https://gif.ski/>`_"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:123
msgid "`LossyGif <https://kornel.ski/lossygif>`_"
msgstr "`LossyGif <https://kornel.ski/lossygif>`_"

#: ../../contributors_manual/optimising_images.rst:126
msgid "Optimising JPEG"
msgstr "Optimitzar els JPEG"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:128
msgid ""
"Now, JPEG is really tricky to optimize properly. This is because it is a :"
"ref:`lossy file format <lossy_compression>`, and that means that it uses "
"psychological tricks to store its data."
msgstr ""
"Ara, el JPEG és realment difícil d'optimitzar adequadament. Això es deu al "
"fet que és un :ref:`format de fitxer amb pèrdua <lossy_compression>`, i vol "
"dir que utilitza trucs psicològics per a emmagatzemar les seves dades."

#: ../../contributors_manual/optimising_images.rst:130
msgid ""
"However, tricks like these become very obvious when your image has a lot of "
"contrast, like text. Furthermore, JPEGs don't do well when they are resaved "
"over and over. Therefore, make sure that there's a lossless version of the "
"image somewhere that you can edit, and that only the final result is in JPEG "
"and gets compressed further."
msgstr ""
"No obstant això, trucs com aquests es tornen molt obvis quan la vostra "
"imatge té molt de contrast, com el text. A més, els fitxers JPEG no "
"funcionen bé quan es tornen a desar una i altra vegada. Per tant, assegureu-"
"vos que hi hagi una versió sense pèrdua de la imatge en algun lloc que "
"pugueu editar, i que només el resultat final estigui en JPEG i es "
"comprimeixi encara més."

#: ../../contributors_manual/optimising_images.rst:135
msgid "MacOS/ OS X"
msgstr "MacOS/ OS X"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:137
msgid ""
"`ImageOptim <https://imageoptim.com/mac>`_ -- A Graphical User Interface "
"wrapper around commandline tools like PNGquant and gifski."
msgstr ""
"`ImageOptim <https://imageoptim.com/mac>`_ -- Un embolcall de la interfície "
"gràfica d'usuari al voltant de les eines de línia d'ordres com PNGquant i "
"gifski."

#: ../../contributors_manual/optimising_images.rst:140
msgid "Editing the metadata of a file"
msgstr "Editar les metadades d'un fitxer"

#: ../../contributors_manual/optimising_images.rst:142
msgid ""
"Sometimes, personal information gets embedded into an image file. "
"Othertimes, we want to embed information into a file to document it better."
msgstr ""
"De vegades, la informació personal s'incrusta en un fitxer d'imatge. Altres "
"vegades, volem incrustar informació en un fitxer per a documentar-lo millor."

#: ../../contributors_manual/optimising_images.rst:144
msgid ""
"There are no less than 3 to 4 different ways of handling metadata, and "
"metadata has different ways of handling certain files."
msgstr ""
"No hi ha menys de 3 a 4 formes diferents de gestionar les metadades, i les "
"metadades tenen diferents maneres de gestionar-se en certs fitxers."

# skip-rule: t-acc_obe,t-sp_2p,t-2p_sp,t-apo_fin
#: ../../contributors_manual/optimising_images.rst:146
msgid ""
"The most commonly used tool to edit metadata is :program:`ExifTool`, another "
"is to use :program:`ImageMagick`."
msgstr ""
"L'eina més utilitzada per editar les metadades és :program:`ExifTool`, una "
"altra és utilitzar l' :program:`ImageMagick`."

#: ../../contributors_manual/optimising_images.rst:149
msgid "Windows and OS X"
msgstr "Windows i OS X"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:151
msgid ""
"To get exiftool, `just get it from the website <https://www.sno.phy.queensu."
"ca/~phil/exiftool/>`_."
msgstr ""
"Per obtenir l'ExifTool, `simplement obteniu-lo des del lloc web <https://www."
"sno.phy.queensu.ca/~phil/exiftool/>`_."

#: ../../contributors_manual/optimising_images.rst:156
msgid "On Linux, you can also install exiftool."
msgstr "A Linux, també podeu instal·lar l'ExifTool."

#: ../../contributors_manual/optimising_images.rst:159
msgid "Debian/Ubuntu"
msgstr "Debian/Ubuntu"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:159
msgid "``sudo apt-get install libimage-exiftool-perl``"
msgstr "``sudo apt-get install libimage-exiftool-perl``"

#: ../../contributors_manual/optimising_images.rst:162
msgid "Viewing Metadata"
msgstr "Visualitzar les metadades"

#: ../../contributors_manual/optimising_images.rst:164
msgid ""
"Change the directory to the folder where the image is located and type::"
msgstr "Canvieu el directori a la carpeta on es troba la imatge i escriviu::"

#: ../../contributors_manual/optimising_images.rst:166
msgid "exiftool image"
msgstr "exiftool imatge"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:168
msgid ""
"where image is the file you'd like to examine. If you just type ``exiftool`` "
"in any given folder it will output all the information it can give about any "
"file it comes across. If you take a good look at some images, you'll see "
"they contain author or location metadata. This can be a bit of a problem "
"sometimes when it comes to privacy, and also the primary reason all metadata "
"gets stripped."
msgstr ""
"On «*imatge*» és el fitxer que us agradaria examinar. Si només escriviu "
"``exiftool`` en una carpeta determinada, es generarà tota la informació que "
"pugui proporcionar sobre qualsevol fitxer que trobi. Si observeu bé algunes "
"imatges, veureu que contenen metadades sobre l'autor o ubicació. De vegades, "
"això pot ser una mica problemàtic quan es tracta de privacitat, i també és "
"la raó principal per la qual es descarten totes les metadades."

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:170
msgid ""
"You can also use `ImageMagick's identify <https://www.imagemagick.org/script/"
"identify.php>`_::"
msgstr ""
"També podeu utilitzar l'ordre `«identify» de l'ImageMagick <https://www."
"imagemagick.org/script/identify.php>`_::"

#: ../../contributors_manual/optimising_images.rst:172
msgid "identify -verbose image"
msgstr "identify -verbose imatge"

#: ../../contributors_manual/optimising_images.rst:175
msgid "Stripping Metadata"
msgstr "Descartar les metadades"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:177
msgid ""
"Stripping metadata from the example ``image.png`` can be done as follows:"
msgstr ""
"Descartar les metadades de l'exemple ``imatge.png`` es pot fer de la següent "
"manera:"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:180
msgid "`exiftool -all= image.png`"
msgstr "`exiftool -all= imatge.png`"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:182
msgid ""
"`ExifTool <http://www.linux-magazine.com/Online/Blogs/Productivity-Sauce/"
"Remove-EXIF-Metadata-from-Photos-with-exiftool>`_"
msgstr ""
"`ExifTool <http://www.linux-magazine.com/Online/Blogs/Productivity-Sauce/"
"Remove-EXIF-Metadata-from-Photos-with-exiftool>`_"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:182
msgid ""
"This empties all tags exiftool can get to. You can also be specific and only "
"remove a single tag: `exiftool -author= image.png`"
msgstr ""
"Això buidarà totes les etiquetes a les quals pugui accedir l'ExifTool. També "
"podeu ser específics i només eliminar una sola etiqueta: `exiftool -"
"author=imatge.png`"

#: ../../contributors_manual/optimising_images.rst:185
msgid "OptiPNG"
msgstr "OptiPNG"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:185
msgid "`optipng -strip image.png` This will strip and compress the png file."
msgstr ""
"`optipng -strip imatge.png` Això descartarà dades i comprimirà el fitxer PNG."

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:188
msgid ""
"`ImageMagick <https://www.imagemagick.org/script/command-line-options."
"php#strip>`_"
msgstr ""
"`ImageMagick <https://www.imagemagick.org/script/command-line-options."
"php#strip>`_"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:188
msgid "`convert image.png --strip`"
msgstr "`convert imatge.png --strip`"

#: ../../contributors_manual/optimising_images.rst:191
msgid "Extracting metadata"
msgstr "Extreure les metadades"

#: ../../contributors_manual/optimising_images.rst:193
msgid ""
"Sometimes we want to extract metadata, like an icc profile, before stripping "
"everything. This is done by converting the image to the profile type:"
msgstr ""
"De vegades volem extreure les metadades, com un perfil icc, abans de "
"descartar-ho tot. Això es fa convertint la imatge al tipus de perfil:"

#: ../../contributors_manual/optimising_images.rst:196
msgid "First extract the metadata to a profile by converting::"
msgstr "Primer extraieu les metadades a un perfil fent la conversió::"

#: ../../contributors_manual/optimising_images.rst:198
msgid "convert image.png image_profile.icc"
msgstr "convert imatge.png perfil_imatge.icc"

#: ../../contributors_manual/optimising_images.rst:200
msgid "Then strip the file and readd the profile information::"
msgstr ""
"Després descarteu dades del fitxer i torneu a afegir la informació del "
"perfil::"

#: ../../contributors_manual/optimising_images.rst:202
msgid "convert -profile image_profile.icc image.png"
msgstr "convert -profile perfil_imatge.icc imatge.png"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:203
msgid ""
"`ImageMagick's Convert <https://imagemagick.org/script/command-line-options."
"php#profile>`_"
msgstr ""
"L'ordre `«convert» de l'ImageMagick <https://imagemagick.org/script/command-"
"line-options.php#profile>`_"

#: ../../contributors_manual/optimising_images.rst:206
msgid "Embedding description metadata"
msgstr "Incrustar les metadades de descripció"

#: ../../contributors_manual/optimising_images.rst:208
msgid ""
"Description metadata is really useful for the purpose of helping people with "
"screenreaders. Webbrowsers will often try to use the description metadata if "
"there's no alt text to generate the alt-text. Another thing that you might "
"want to embed is stuff like color space data."
msgstr ""
"Les metadades de descripció són realment útils per ajudar a les persones amb "
"lectors de pantalla. Els navegadors web sovint intentaran emprar-les si no "
"hi ha text alternatiu per a generar-lo. Altres coses que potser voldreu "
"incrustar són coses com dades sobre l'espai de color."

#: ../../contributors_manual/optimising_images.rst:210
msgid "ExifTool"
msgstr "ExifTool"

#: ../../contributors_manual/optimising_images.rst:213
msgid "Setting an exif value::"
msgstr "Establir un valor Exif::"

#: ../../contributors_manual/optimising_images.rst:215
msgid ""
"convert -set exif:ImageDescription \"An image description\" image.png "
"image_modified.png"
msgstr ""
"convert -set exif:ImageDescription \"Una descripció de la imatge\" imatge."
"png imatge_modificada.png"

#: ../../contributors_manual/optimising_images.rst:217
msgid "Setting the PNG chunk for description::"
msgstr "Establir el fragment PNG per a la descripció::"

#: ../../contributors_manual/optimising_images.rst:219
msgid ""
"convert -set Description \"An image description\" image.png image_modified."
"png"
msgstr ""
"convert -set Description \"Una descripció de la imatge\" imatge.png "
"imatge_modificada.png"

#: ../../contributors_manual/optimising_images.rst:222
msgid "Embedding license metadata"
msgstr "Incrustar les de metadades de la llicència"

#: ../../contributors_manual/optimising_images.rst:224
msgid ""
"In a certain way, embedding license metadata is really nice because it "
"allows you to permanently mark the image as such. However, if someone then "
"uploads it to another website, it is very likely the metadata is stripped "
"with imagemagick."
msgstr ""
"De certa manera, incrustar les metadades de la llicència és realment "
"agradable perquè permet marcar la imatge de forma permanent. No obstant "
"això, si algú la puja en un altre lloc web, és molt probable que les "
"metadades es descartin amb ImageMagick."

#: ../../contributors_manual/optimising_images.rst:227
msgid "Using Properties"
msgstr "Utilitzar les propietats"

#: ../../contributors_manual/optimising_images.rst:229
msgid ""
"You can use dcterms:license for defining the document where the license is "
"defined."
msgstr ""
"Podeu utilitzar «dcterms:license» per a definir el document on s'aplica la "
"llicència."

#: ../../contributors_manual/optimising_images.rst:232
msgid "For the GDPL::"
msgstr "Per a la GDPL::"

#: ../../contributors_manual/optimising_images.rst:234
msgid ""
"convert -set dcterms:license \"GDPL 1.3+ https://www.gnu.org/licenses/"
"fdl-1.3.txt\" image.png"
msgstr ""
"convert -set dcterms:license \"GDPL 1.3+ https://www.gnu.org/licenses/"
"fdl-1.3.txt\" imatge.png"

#: ../../contributors_manual/optimising_images.rst:236
msgid "This defines a shorthand name and then license text."
msgstr "Això defineix un nom abreujat i després el text de la llicència."

#: ../../contributors_manual/optimising_images.rst:238
msgid "For Creative Commons BY-SA 4.0::"
msgstr "Per a la Creative Commons BY-SA 4.0::"

#: ../../contributors_manual/optimising_images.rst:240
msgid ""
"convert -set dcterms:license \"CC-BY-SA-4.0 https://creativecommons.org/"
"licenses/by-sa/4.0/\" image.png"
msgstr ""
"convert -set dcterms:license \"CC-BY-SA-4.0 https://creativecommons.org/"
"licenses/by-sa/4.0/\" imatge.png"

#: ../../contributors_manual/optimising_images.rst:242
msgid ""
"The problem with using properties is that they are a non-standard way to "
"define a license, meaning that machines cannot do much with them."
msgstr ""
"El problema d'utilitzar les propietats és que són una manera no estàndard de "
"definir una llicència, de manera que les màquines no podran fer gaire amb "
"elles."

#: ../../contributors_manual/optimising_images.rst:245
msgid "Using XMP"
msgstr "Utilitzar el XMP"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:247
msgid ""
"The creative commons website suggest we `use XMP for this <https://wiki."
"creativecommons.org/wiki/XMP>`_. You can ask the Creative Commons License "
"choose to generate an appropriate XMP file for you when picking a license."
msgstr ""
"El lloc web de Creative Commons suggereix que `utilitzem XMP per a això "
"<https://wiki.creativecommons.org/wiki/XMP>`_. Pot sol·licitar que la "
"llicència Creative Commons triï generar un fitxer XMP adequat mentre trieu "
"una llicència."

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:249
msgid ""
"We'll need to use the `XMP tags for exiftool <https://www.sno.phy.queensu.ca/"
"~phil/exiftool/TagNames/XMP.html>`_."
msgstr ""
"Haurem d'utilitzar les `etiquetes XMP per a l'ExifTool <https://www.sno.phy."
"queensu.ca/~phil/exiftool/TagNames/XMP.html>`_."

#: ../../contributors_manual/optimising_images.rst:251
msgid "So that would look something like this::"
msgstr "Així doncs, veurem alguna cosa així::"

# skip-rule: t-pu_sp
#: ../../contributors_manual/optimising_images.rst:253
msgid ""
"exiftool -Marked=true -License=\"https://creativecommons.org/licenses/by-"
"sa/4.0\" -UsageTerms=\"This work is licensed under a <a rel=\"license\" href="
"\"https://creativecommons.org/licenses/by-sa/4.0/\">Creative Commons "
"Attribution-ShareAlike 4.0 International License</a>.\" -Copyright=\"CC-BY-"
"SA-NC 4.0\" image.png"
msgstr ""
"exiftool -Marked=true -License=\"https://creativecommons.org/licenses/by-"
"sa/4.0\" -UsageTerms=\"This work is licensed under a <a rel=\"license\" href="
"\"https://creativecommons.org/licenses/by-sa/4.0/\">Creative Commons "
"Attribution-ShareAlike 4.0 International License</a>.\" -Copyright=\"CC-BY-"
"SA-NC 4.0\" imatge.png"

#: ../../contributors_manual/optimising_images.rst:255
msgid "Another way of doing the marking is::"
msgstr "Una altra manera de fer el marcatge és::"

#: ../../contributors_manual/optimising_images.rst:257
msgid ""
"exiftool -Marked=true -License=\"https://creativecommons.org/licenses/by-"
"sa/4.0\" -attributionURL=\"docs.krita.org\" attributionName=\"kritaManual\" "
"image.png"
msgstr ""
"exiftool -Marked=true -License=\"https://creativecommons.org/licenses/by-"
"sa/4.0\" -attributionURL=\"docs.krita.org\" attributionName=\"kritaManual\" "
"imatge.png"

#: ../../contributors_manual/optimising_images.rst:260
msgid "First extract the data (if there is any)::"
msgstr "Primer extraieu les dades (si n'hi ha)::"

# skip-rule: t-acc_obe
#: ../../contributors_manual/optimising_images.rst:262
msgid "convert image.png image_meta.xmp"
msgstr "convert imatge.png metadades_imatge.xmp"

#: ../../contributors_manual/optimising_images.rst:264
msgid "Then modify the resulting file, and embed the image data::"
msgstr ""
"Després modifiqueu el fitxer resultant i incrusteu les dades a la imatge::"

#: ../../contributors_manual/optimising_images.rst:266
msgid "With imagemagick you can use the profile option again."
msgstr "Amb l'ImageMagick podreu tornar a utilitzar l'opció del perfil."

#: ../../contributors_manual/optimising_images.rst:266
msgid "convert -profile image_meta.xmp image.png"
msgstr "convert -profile metadades_imatge.xmp imatge.png"

#: ../../contributors_manual/optimising_images.rst:268
msgid ""
"The XMP definitions per license. You can generate an XMP file for the "
"metadata on the creative commons website."
msgstr ""
"Les definicions XMP per llicència. Podeu generar un fitxer XMP per a les "
"metadades en el lloc web de Creative Commons."
