msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-02-27 01:38+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""

#: ../../reference_manual/dockers/overview.rst:1
msgid "Overview of the overview docker."
msgstr ""

#: ../../reference_manual/dockers/overview.rst:11
#: ../../reference_manual/dockers/overview.rst:16
msgid "Overview"
msgstr ""

#: ../../reference_manual/dockers/overview.rst:11
msgid "Navigation"
msgstr ""

#: ../../reference_manual/dockers/overview.rst:19
msgid ".. image:: images/dockers/Krita_Overview_Docker.png"
msgstr ""

#: ../../reference_manual/dockers/overview.rst:20
msgid ""
"This docker allows you to see a full overview of your image. You can also "
"use it to navigate and zoom in and out quickly. Dragging the view-rectangle "
"allows you quickly move the view."
msgstr ""

#: ../../reference_manual/dockers/overview.rst:22
msgid ""
"There are furthermore basic navigation functions: Dragging the zoom-slider "
"allows you quickly change the zoom."
msgstr ""

#: ../../reference_manual/dockers/overview.rst:26
msgid ""
"Toggling the mirror button will allow you to mirror the view of the canvas "
"(but not the full image itself) and dragging the rotate slider allows you to "
"adjust the rotation of the viewport. To reset the rotation, |mouseright| the "
"slider to edit the number, and type '0'."
msgstr ""
