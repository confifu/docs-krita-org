# Translation of docs_krita_org_reference_manual___tools___rectangle.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___rectangle\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:44+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Ratio"
msgstr "Пропорція"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:26
msgid ""
".. image:: images/icons/rectangle_tool.svg\n"
"   :alt: toolrectangle"
msgstr ""
".. image:: images/icons/rectangle_tool.svg\n"
"   :alt: toolrectangle"

#: ../../reference_manual/tools/rectangle.rst:1
msgid "Krita's rectangle tool reference."
msgstr "Довідник з інструмента малювання прямокутників у Krita."

#: ../../reference_manual/tools/rectangle.rst:10
msgid "Tools"
msgstr "Інструменти"

#: ../../reference_manual/tools/rectangle.rst:10
msgid "Rectangle"
msgstr "Прямокутник"

#: ../../reference_manual/tools/rectangle.rst:15
msgid "Rectangle Tool"
msgstr "Інструмент «Прямокутник»"

#: ../../reference_manual/tools/rectangle.rst:17
msgid "|toolrectangle|"
msgstr "|toolrectangle|"

#: ../../reference_manual/tools/rectangle.rst:19
msgid ""
"This tool can be used to paint rectangles, or create rectangle shapes on a "
"vector layer. Click and hold |mouseleft| to indicate one corner of the "
"rectangle, drag to the opposite corner, and release the button."
msgstr ""
"За допомогою цього інструмента можна малювати прямокутники або створювати "
"прямокутні форми у векторних шарах. Натисніть і утримуйте натиснутою |"
"mouseleft| для позначення одного з кутів прямокутника, потім перетягніть "
"вказівник у протилежний його кут і відпустіть кнопку миші."

#: ../../reference_manual/tools/rectangle.rst:22
msgid "Hotkeys and Sticky-keys"
msgstr "Керування за допомогою клавіатури"

#: ../../reference_manual/tools/rectangle.rst:24
msgid "There's no default hotkey for switching to rectangle."
msgstr ""
"Типової комбінації клавіш для перемикання на інструмент малювання "
"прямокутників не передбачено."

#: ../../reference_manual/tools/rectangle.rst:26
msgid ""
"If you hold the :kbd:`Shift` key while drawing, a square will be drawn "
"instead of a rectangle. Holding the :kbd:`Ctrl` key will change the way the "
"rectangle is constructed. Normally, the first mouse click indicates one "
"corner and the second click the opposite. With the :kbd:`Ctrl` key, the "
"initial mouse position indicates the center of the rectangle, and the final "
"mouse position indicates a corner. You can press the :kbd:`Alt` key while "
"still keeping |mouseleft| down to move the rectangle to a different location."
msgstr ""
"Якщо під час малювання ви утримуватимете натиснутою клавішу :kbd:`Shift`, "
"програма намалює квадрат замість прямокутника. Натискання і утримування "
"клавіші :kbd:`Ctrl` змінює спосіб, у який програма будуватиме прямокутник. У "
"звичайному режимі перше клацання кнопкою миші позначає один з кутів "
"прямокутника, а відпускання кнопки — протилежний кут. Якщо натиснуто "
"клавішу :kbd:`Ctrl`, програма вважаєте точку першого клацання центром "
"прямокутника, а остаточну позицію вказівника — розташуванням одного з його "
"кутів. Ви також можете натиснути клавішу :kbd:`Alt`, утримуючи натиснутою |"
"mouseleft|, щоб пересунути прямокутник на інше місце."

#: ../../reference_manual/tools/rectangle.rst:28
msgid ""
"You can change between the corner/corner and center/corner drawing methods "
"as often as you want by pressing or releasing the :kbd:`Ctrl` key, provided "
"that you keep |mouseleft| pressed. With the :kbd:`Ctrl` key pressed, mouse "
"movements will affect all four corners of the rectangle (relative to the "
"center), without the :kbd:`Ctrl` key, one of the corners is unaffected."
msgstr ""
"Якщо захочете, можете перемикатися між режимами малювання кут-кут та центр-"
"кут натисканням і відпусканням клавіші :kbd:`Ctrl`, доки лишається "
"натиснутою |mouseleft|. Якщо натиснуто клавішу :kbd:`Ctrl`, пересування "
"вказівника миші стосуватиметься усіх чотирьох кутів прямокутника (відносно "
"центру), якщо клавішу :kbd:`Ctrl` не натиснуто — розташування одного з кутів "
"не змінюватиметься."

#: ../../reference_manual/tools/rectangle.rst:32
msgid "Tool Options"
msgstr "Параметри інструмента"

#: ../../reference_manual/tools/rectangle.rst:35
msgid "Fill"
msgstr "Заповнити"

#: ../../reference_manual/tools/rectangle.rst:37
msgid "Not filled"
msgstr "Без заповнення"

#: ../../reference_manual/tools/rectangle.rst:38
msgid "The rectangle will be transparent from the inside."
msgstr "Прямокутник буде прозорим всередині."

#: ../../reference_manual/tools/rectangle.rst:39
msgid "Foreground color"
msgstr "Колір тексту"

#: ../../reference_manual/tools/rectangle.rst:40
msgid "The rectangle will use the foreground color as fill."
msgstr "Для заповнення прямокутника буде використано колір переднього плану."

#: ../../reference_manual/tools/rectangle.rst:41
msgid "Background color"
msgstr "Колір тла"

#: ../../reference_manual/tools/rectangle.rst:42
msgid "The rectangle will use the background color as fill."
msgstr "Для заповнення прямокутника буде використано колір тла."

#: ../../reference_manual/tools/rectangle.rst:44
msgid "Pattern"
msgstr "Візерунок"

#: ../../reference_manual/tools/rectangle.rst:44
msgid "The rectangle will use the active pattern as fill."
msgstr "Для прямокутника буде використано активний візерунок для заповнення."

#: ../../reference_manual/tools/rectangle.rst:47
msgid "Outline"
msgstr "Контур"

#: ../../reference_manual/tools/rectangle.rst:49
msgid "No Outline"
msgstr "Без контуру"

#: ../../reference_manual/tools/rectangle.rst:50
msgid "The Rectangle will render without outline."
msgstr "Прямокутник буде показано без контуру."

#: ../../reference_manual/tools/rectangle.rst:52
msgid "Brush"
msgstr "Пензель"

#: ../../reference_manual/tools/rectangle.rst:52
msgid "The Rectangle will use the current selected brush to outline."
msgstr ""
"Прямокутник використовуватиме поточний позначений пензель для малювання "
"контуру."

#: ../../reference_manual/tools/rectangle.rst:55
msgid ""
"On vector layers, the rectangle will not render with a brush outline, but "
"rather a vector outline."
msgstr ""
"На векторних шарах межі прямокутника буде намальовано у векторному стилі, а "
"не за допомогою поточного пензля."

#: ../../reference_manual/tools/rectangle.rst:57
msgid "Anti-aliasing"
msgstr "Згладжування"

#: ../../reference_manual/tools/rectangle.rst:58
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
"Цей пункт визначає, чи буде програма згладжувати межі позначеної ділянки. "
"Дехто надає перевагу різким межам позначених ділянок."

#: ../../reference_manual/tools/rectangle.rst:59
msgid "Width"
msgstr "Ширина"

#: ../../reference_manual/tools/rectangle.rst:60
msgid ""
"Gives the current width. Use the lock to force the next selection made to "
"this width."
msgstr ""
"Задає поточну ширину. Скористайтеся кнопкою із замочком, щоб примусово "
"задати для наступного позначення цю ширину."

#: ../../reference_manual/tools/rectangle.rst:61
msgid "Height"
msgstr "Висота"

#: ../../reference_manual/tools/rectangle.rst:62
msgid ""
"Gives the current height. Use the lock to force the next selection made to "
"this height."
msgstr ""
"Задає поточну висоту. Скористайтеся кнопкою із замочком, щоб примусово "
"задати для наступного позначення цю висоту."

#: ../../reference_manual/tools/rectangle.rst:66
msgid ""
"Gives the current ratio. Use the lock to force the next selection made to "
"this ratio."
msgstr ""
"Задає поточне співвідношення розмірів. Скористайтеся кнопкою із замочком, "
"щоб примусово задати для наступного позначення це співвідношення розміру."

#: ../../reference_manual/tools/rectangle.rst:68
msgid "Round X"
msgstr "Округлення за X"

#: ../../reference_manual/tools/rectangle.rst:70
msgid "The horizontal radius of the rectangle corners."
msgstr "Горизонтальна напіввісь для згладжування кутів прямокутника."

#: ../../reference_manual/tools/rectangle.rst:72
msgid "Round Y"
msgstr "Округлення за Y"

#: ../../reference_manual/tools/rectangle.rst:74
msgid "The vertical radius of the rectangle corners."
msgstr "Вертикальна напіввісь для згладжування кутів прямокутника."
