msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___main_menu___image_menu.pot\n"

#: ../../<generated>:1
msgid "Separate Image"
msgstr "分离图像"

#: ../../reference_manual/main_menu/image_menu.rst:1
msgid "The image menu in Krita."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Image"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Canvas Projection Color"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Trim"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Resize"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Scale"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Mirror"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Transform"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Convert Color Space"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Offset"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Split Channels"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:16
msgid "Image Menu"
msgstr "图像菜单"

#: ../../reference_manual/main_menu/image_menu.rst:18
msgid "Properties"
msgstr "属性"

#: ../../reference_manual/main_menu/image_menu.rst:19
msgid "Gives you the image properties."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:20
msgid "Image Background Color and Transparency"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:21
msgid "Change the background canvas color."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:22
msgid "Convert Current Image Color Space."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:23
msgid "Converts the current image to a new colorspace."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:24
msgid "Trim to image size"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:25
msgid ""
"Trims all layers to the image size. Useful for reducing filesize at the loss "
"of information."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:26
msgid "Trim to Current Layer"
msgstr "裁剪到当前图层"

#: ../../reference_manual/main_menu/image_menu.rst:27
msgid ""
"A lazy cropping function. Krita will use the size of the current layer to "
"determine where to crop."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:28
msgid "Trim to Selection"
msgstr "剪裁到选区"

#: ../../reference_manual/main_menu/image_menu.rst:29
msgid ""
"A lazy cropping function. Krita will crop the canvas to the selected area."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:30
msgid "Rotate Image"
msgstr "旋转图像"

#: ../../reference_manual/main_menu/image_menu.rst:31
msgid "Rotate the image"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:32
msgid "Shear Image"
msgstr "裁剪图像"

#: ../../reference_manual/main_menu/image_menu.rst:33
msgid "Shear the image"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:34
msgid "Mirror Image Horizontally"
msgstr "水平镜像图像"

#: ../../reference_manual/main_menu/image_menu.rst:35
msgid "Mirror the image on the horizontal axis."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:36
msgid "Mirror Image Vertically"
msgstr "垂直镜像图像"

#: ../../reference_manual/main_menu/image_menu.rst:37
msgid "Mirror the image on the vertical axis."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:38
msgid "Scale to New Size"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:39
msgid ""
"The resize function in any other program with the :kbd:`Ctrl + Alt + I` "
"shortcut."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:40
msgid "Offset Image"
msgstr "偏移图像"

#: ../../reference_manual/main_menu/image_menu.rst:41
msgid "Offset all layers."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:42
msgid "Resize Canvas"
msgstr "调整画布大小"

#: ../../reference_manual/main_menu/image_menu.rst:43
msgid "Change the canvas size. Don't confuse this with Scale to new size."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:44
msgid "Image Split"
msgstr "图像分割"

#: ../../reference_manual/main_menu/image_menu.rst:45
msgid "Calls up the :ref:`image_split` dialog."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:46
msgid "Wavelet Decompose"
msgstr "小波分解"

#: ../../reference_manual/main_menu/image_menu.rst:47
msgid "Does :ref:`wavelet_decompose` on the current layer."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:49
msgid ":ref:`Separates <separate_image>` the image into channels."
msgstr ""
